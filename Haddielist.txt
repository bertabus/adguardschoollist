# AdAway/adguard blocklist
# [block youtube]
||youtube.com^$client='Haddie-Chromebook' 
||googlevideo.com^$client='Haddie-Chromebook'
||ytimg.com^$client='Haddie-Chromebook'
||youtube-ui.l.google.com^$client='Haddie-Chromebook'
||ytimg.l.google.com^$client='Haddie-Chromebook'
||ytstatic.l.google.com^$client='Haddie-Chromebook'
||youtubei.googleapis.com^$client='Haddie-Chromebook'

# [block plex]
||app.plex.tv^$client='Haddie-Chromebook'

# [block Netflix]
||netflix.com^$client='Haddie-Chromebook'

# [block minecraft]
||msgamestudios.com^$client='Haddie-Chromebook'

# [other games]
||paper-io.com^$client='Haddie-Chromebook'
